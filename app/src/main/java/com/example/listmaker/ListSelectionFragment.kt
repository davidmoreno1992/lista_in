package com.example.listmaker

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


class ListSelectionFragment : Fragment(),ListSelectionListener {


    private var listener: OnFragmentInteractionListener? = null
    lateinit var listsRecyclerView: RecyclerView
    lateinit var listDataManager: ListDataManager



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val lists = listDataManager.readLists()
        view?.let {
            listsRecyclerView = it.findViewById(R.id.lists_recyclerview)
            listsRecyclerView.layoutManager = LinearLayoutManager(activity)
            listsRecyclerView.adapter = ListsRecyclerViewAdapter(lists, this)
        }

    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_selection, container, false)
    }

   override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
            listDataManager = ListDataManager(context)
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun listItemSelected(list: TaskList) {
        listener?.onListItemSelected(list)
    }

    interface OnFragmentInteractionListener {
        fun  onListItemSelected(list:TaskList)
    }

    fun addList(list: TaskList)
    {
        listDataManager.saveList()
        val adapter=listsRecyclerView.adapter as ListsRecyclerViewAdapter
        adapter.addList(list)
    }
    fun saveList(list: TaskList)
    {
        listDataManager.saveList(list)
        updateLists()
    }


    private fun updateLists() {
        val lists = listDataManager.readLists()
        listsRecyclerView.adapter = ListsRecyclerViewAdapter(lists, this)
    }

    companion object {

    fun newIntance():ListSelectionFragment{
    val fragment=ListSelectionFragment()
        return fragment
 }
    }
}
